# pihole-template-chart

A Helm chart to deploy Pi-Hole to Kubernetes with persistent storage. As I prefer to use ``helm template`` instead of ``helm install``, this may contain modifications that break things like ``helm upgrade``.
